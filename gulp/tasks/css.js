var gulp = require("gulp"),
    watch=require("gulp-watch"),
    postcss= require("gulp-postcss"),
    autoprefixer=require("autoprefixer"),
    cssvars=require("postcss-simple-vars"),
    nested=require("postcss-nested"),
    cssImport = require("postcss-import"),

    mixins=require("postcss-mixins");
	hexrgba = require("postcss-hexrgba");

    mixins=require("postcss-mixins"),
	hexrgba=require("postcss-hexrgba");

gulp.task("css",function(){
//    return gulp.src("./app/assets/styles/style.css")
//    .pipe(postcss([cssvars,nested,autoprefixer]))
//    .pipe(gulp.dest("./app/test/styles"));
    
    return gulp.src("./app/assets/styles/style.css")
    .pipe(postcss([cssImport,mixins,cssvars,nested,hexrgba,autoprefixer]))
    .pipe(gulp.dest("./app/css/"));
});